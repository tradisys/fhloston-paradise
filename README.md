# Fhloston Paradice

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run tests for smart contract (standalone project)
##### Test flow will deploy smart contract for whole gaming cycle. You have to define your own TestFlow host.

`smart-contract/tests/test/fomo.test.ts`
```
const flow = new TestFlow('YOUR_SEED', TestNetApi)
```
##### Then choose network for tests: `TestNetApi` or `MainNetApi`
##### `TestFlow` will create necessary addresses, spread WAVES between them and use in test flow.
##### After all, `TestFlow` will return all WAVES back, to host address.
```
cd smart-contract/test
npm install
npm jest --verbose
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Fhloston Paradise Smart Contract details

```
Address: 3P22HSYExcKjPSLSK8ZEdysZwmWN42PDUfU
Public Key: DJZ6eQy1MC87ZFN5vHFjJhLxKnwvsDizmBtktHMaU9zQ
Private Key: 6RLA4Pnj1xNgRCH3qUYjzkxuppPGLQ7gt3dgotj9wqjg
Seed: noise useful loyal return pilot say follow album below nasty galaxy cable next bridge chuckle
```
