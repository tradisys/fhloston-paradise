// Application config
//
// Environment vars should be defined by following method:
// https://cli.vuejs.org/guide/mode-and-env.html

export default {
  app: {
    name: 'Fear of missing out',
  },
  api: {
    network: process.env.VUE_APP_API_NETWORK,
    v0: process.env.VUE_APP_API_V0,
    nodes: process.env.VUE_APP_API_NODES.split(','),
    // endpoints:
    pairWavesWUsd: '/pairs/WAVES/Ft8X1v1LTa1ABafufpaCWyVj8KkaxUWE6xBhW6sNFJck',
    explorer: 'https://wavesexplorer.com/address',
    data: '/addresses/data',
    balance: '/addresses/balance/details',
    balanceConfirmations: '/addresses/balance/',
    transactions: '/transactions/address',
    transactionById: '/transactions/info',
    transactionUnconfirmedById: '/transactions/unconfirmed/info',
    broadcast: '/transactions/broadcast',
    limit: '/limit',
    height: '/blocks/height',
  },
  extensions: {
    chrome: 'https://chrome.google.com/webstore/detail/waves-keeper/lpilbniiabackdjcionkobglmddfbcjo',
    firefox: 'https://addons.mozilla.org/firefox/addon/waves-keeper',
  },
  address: process.env.VUE_APP_ADDRESS,
  publicKey: process.env.VUE_APP_PUBLIC_KEY,
  fields: {
    lastPayment: 'lastPayment',
    heightToGetMoney: 'heightToGetMoney',
  },
  blocksOnGameStart: Number(process.env.VUE_APP_BLOCKS_ON_GAME_START),
  blocksPerRound: Number(process.env.VUE_APP_BLOCKS_PER_ROUND),
  blocksPerCompetition: Number(process.env.VUE_APP_BLOCKS_PER_COMPETITION),
  blocksPerWithdraw: Number(process.env.VUE_APP_BLOCKS_PER_WITHDRAW),
  blocksToIncreaseHeight: Number(process.env.VUE_APP_BLOCKS_TO_INCREASE_HEIGHT),
  getPrizeVisibilityThreshold: 30,
  tableEntries: 10,
  updateFrequency: 2000,
  wave: 100000000,
  txPayment: {
    fee: 0.01,
    amount: 1,
    default: 0.005, // only for Smart Contracts
  },
  txPayout: {
    fee: 500000,
  },
  txData: {
    fee: 0.005,
  },
};
