import config from '@/config';
import urlJoin from 'url-join';
import { firstSuccessful } from '@/utils';

const jsonHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

const fetchPool = (path, init, onlyOk = true) => firstSuccessful(
  config.api.nodes.map(
    node => new Promise(
      ((resolve) => {
        fetch(urlJoin(node, path), init)
          .then((r) => {
            if (onlyOk && r.ok) {
              resolve(r);
            } else {
              resolve(r);
            }
          })
          .catch(() => {
          });
      }),
    ),
  ),
);

export const getAddressStateKey = async (
  address,
  key,
) => {
  const response = await fetchPool(urlJoin(
    config.api.data,
    address,
    key,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};


export const getAddressBalanceInfo = async (
  address,
) => {
  const response = await fetchPool(urlJoin(
    config.api.balance,
    address,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getAddressLastTransferOut = async (
  sender,
  limit = 1,
) => {
  const response = await fetch(urlJoin(
    config.api.v0,
    'transactions',
    'transfer',
    `?sender=${sender}`,
    `&limit=${limit}`,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getAddressTransactionsLimited = async (
  address,
  limit,
) => {
  const response = await fetchPool(urlJoin(
    config.api.transactions,
    address,
    config.api.limit,
    limit,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const getTransactionById = async (
  id,
) => {
  const response = await fetchPool(urlJoin(
    config.api.transactionById,
    id,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  }, false);

  return response.json();
};

export const getTransactionUnconfirmedById = async (
  id,
) => {
  const response = await fetchPool(urlJoin(
    config.api.transactionUnconfirmedById,
    id,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  }, false);

  return response.json();
};

export const getCurrentBlockHeight = async () => {
  const response = await fetchPool(urlJoin(
    config.api.height,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};

export const broadcast = async (data) => {
  const response = await fetchPool(urlJoin(
    config.api.broadcast,
  ), {
    method: 'POST',
    headers: jsonHeaders,
    body: JSON.stringify(data),
  }, false);

  return response.json();
};

export const getLastPriceInUSD = async () => {
  const response = await fetch(urlJoin(
    config.api.v0,
    config.api.pairWavesWUsd,
  ), {
    method: 'GET',
    headers: jsonHeaders,
  });

  return response.json();
};
