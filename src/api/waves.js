// Waves object, provided by WavesKeeperExtension globally.
// The main feature, is that Waves object injects to page not instantly.
// Sometimes it happens, sometimes not.
//
// Here, Waves is simple get function and returns object on demand.
export const Setup = () => {
  let waves;
  let injected = false;
  return () => {
    if (injected) {
      return waves;
    }
    // Kinda like interface with mixed global Waves object:
    waves = {
      auth() {
      },
      signAndPublishTransaction() {
      },
      signTransactionPackage() {
      },
      signTransaction() {
      },
      signDataTransaction() {
      },
      publicState() {
      },
      ...window.Waves,
    };
    injected = true;

    return waves;
  };
};

// Here, Waves is closure, with lazy value initialized inside.
const Waves = Setup();

export const auth = async data => Waves()
  .auth(data);

export const publicState = async () => Waves()
  .publicState();

export const signTransactionPackage = async txs => Waves()
  .signTransactionPackage(txs);

export const signTransaction = async (
  amount,
  fee,
  recipientAddress,
) => {
  const signedTransferTransactionString = await Waves()
    .signTransaction({
      type: 4,
      data: {
        amount: {
          assetId: 'WAVES',
          tokens: amount,
        },
        fee: {
          assetId: 'WAVES',
          tokens: fee,
        },
        recipient: recipientAddress,
      },
    });

  return JSON.parse(signedTransferTransactionString);
};

export const signAndPublishTransaction = async (
  amount,
  fee,
  recipientAddress,
  senderPublicKey,
) => {
  const signedTransactionString = await Waves()
    .signAndPublishTransaction({
      type: 4,
      data: {
        senderPublicKey,
        amount: {
          assetId: 'WAVES',
          tokens: amount,
        },
        fee: {
          assetId: 'WAVES',
          tokens: fee,
        },
        recipient: recipientAddress,
      },
    });

  return JSON.parse(signedTransactionString);
};

export const signAndPublishTransactionPayout = async (
  amount,
  fee,
  recipientAddress,
  senderPublicKey,
) => {
  const signedTransactionString = await Waves()
    .signAndPublishTransaction({
      type: 4,
      data: {
        senderPublicKey,
        amount: {
          assetId: 'WAVES',
          tokens: amount,
        },
        fee: {
          assetId: 'WAVES',
          tokens: fee,
        },
        recipient: recipientAddress,
      },
    });

  console.log(signedTransactionString);

  return JSON.parse(signedTransactionString);
};

export const signDataTransaction = async (data, fee, senderPublicKey) => {
  const signedDataTransactionString = await Waves()
    .signTransaction({
      type: 12,
      data: {
        senderPublicKey,
        fee: {
          assetId: 'WAVES',
          tokens: fee,
        },
        data,
      },
    });

  return JSON.parse(signedDataTransactionString);
};
