import * as Waves from './waves';
import * as Node from './node';
import config from '@/config';

export {
  Waves,
  Node,
};

// Get last payment from the store:
export const getLastPaymentTx = async () => {
  const { value } = await Node.getAddressStateKey(
    config.address,
    config.fields.lastPayment,
  );
  return value;
};

// Get new height to get money from the store:
export const getHeightToGetMoney = async () => {
  const { value } = await Node.getAddressStateKey(
    config.address,
    config.fields.heightToGetMoney,
  );
  return value;
};

// Get available balance address:
export const getAvailableBalance = async (address) => {
  const { available } = await Node.getAddressBalanceInfo(
    address,
  );
  return available;
};

// Get available balance address on confirmations:
export const getAvailableBalanceOnWithdraw = async () => Node.getAddressLastTransferOut(
  config.address,
);

// Get current block height:
export const getCurrentHeight = async () => {
  const { height } = await Node.getCurrentBlockHeight();
  return height;
};

// Get available balance address:
export const getAddressByTxId = async (id) => {
  const { sender } = await Node.getTransactionById(id);
  return sender;
};

// Get 100 last transactions address:
export const getLastPaymentsAddresses = async () => {
  const data = await Node.getAddressTransactionsLimited(
    config.address,
    '100',
  );

  const dataPool = [];
  const dataFlat = data.flat();
  for (let i = 0; i < dataFlat.length; i += 1) {
    if (dataPool.length >= config.tableEntries) {
      break;
    }
    if (dataFlat[i].type === 12) {
      dataPool.push(dataFlat[i]);
    }
  }

  return Promise.all(dataPool
    .map(async x => ({
      key: x.id,
      value: x.proofs[1] || await getAddressByTxId(
        x.data.find(k => k.key === config.fields.lastPayment).value,
      ),
    })))
    .catch(() => [{
      key: new Date().getMilliseconds(),
      value: 'Other addresses are not loaded',
    }]);
};

// Get last price in USD:
export const getLastPriceUSD = async () => {
  const { data: { lastPrice } } = await Node.getLastPriceInUSD();
  return lastPrice || 0;
};
