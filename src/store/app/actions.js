import * as Actions from './actions-types';
import * as Mutations from './mutation-types';
import * as Api from '@/api';
import {
  transfer as createTransfer,
  data as createData,
} from '@waves/waves-transactions';
import config from '@/config';
import {
  Popups,
  GameStage,
  roundFromHeight,
  wait,
} from '@/utils';

export default {
  //
  // Actions can be invoked from components:
  //
  async startLifecycle({ commit, dispatch, getters }) {
    try {
      const heightToGetMoney = await Api.getHeightToGetMoney();
      commit(Mutations.SET_HEIGHT_TO_GET_MONEY, heightToGetMoney);

      const heightCurrent = await Api.getCurrentHeight();
      commit(Mutations.SET_HEIGHT_CURRENT, heightCurrent);

      const round = roundFromHeight(heightCurrent);
      commit(Mutations.SET_GAME_ROUND, round);

      // Update game stage:
      await dispatch(Actions.UPDATE_GAME_STAGE);
      await dispatch(Actions.UPDATE_COUNTDOWN);

      const lastPaymentTx = await Api.getLastPaymentTx();
      commit(Mutations.SET_LAST_PAYMENT_TX, lastPaymentTx);

      const lastPaymentAddress = await Api.getAddressByTxId(lastPaymentTx);
      commit(Mutations.SET_LAST_PAYMENT_ADDRESS, lastPaymentAddress);

      const lastPaymentsAddresses = await Api.getLastPaymentsAddresses();
      commit(Mutations.SET_LAST_PAYMENTS_ADDRESSES, lastPaymentsAddresses);

      const balance = await Api.getAvailableBalance(config.address);
      commit(Mutations.SET_BALANCE, balance);

      const lastPrice = await Api.getLastPriceUSD();
      commit(Mutations.SET_LAST_PRICE_USD, lastPrice);

      // Check the data txs pool:
      const { dataTxPool } = getters;
      dataTxPool.forEach(dtx => dispatch(Actions.CONTINUE_PUSH_DATA, dtx));
    } catch (e) {
      console.error(e);
    } finally {
      dispatch(Actions.LIFECYCLE);
    }
  },
  async play({ commit, dispatch, getters }) {
    // Process the game:
    try {
      const isValid = await dispatch(Actions.KEEPER_VALIDATION);

      if (isValid) {
        const available = await Api.getAvailableBalance(getters.playerAddress);
        const necessary = (
          config.txPayment.amount * config.wave
          + config.txPayment.fee * config.wave
          + config.txData.fee * config.wave
          + config.txData.fee * config.wave
        );
        if (available < necessary) {
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups.INSUFFICIENT_FUNDS,
            visible: true,
          });
          // Do not proceed due to insufficient funds:
          return;
        }

        commit(Mutations.SET_PROCESSING, true);
        await dispatch(Actions.PLAY_PROCESSING);
      }
    } catch (e) {
      console.error(e);
    } finally {
      commit(Mutations.SET_PROCESSING, false);
    }
  },
  async setLoadingImages({ commit }, images) {
    let counter = 0;
    Array.from(images)
      .forEach((image) => {
        image.addEventListener('load', () => {
          counter += 1;
          commit(Mutations.SET_LOADING_PROGRESS, Math.trunc((counter / images.length) * 100));
        }, false);
      });
  },
  async setWavesKeeperInitialization({ commit }) {
    // If Waves object initialized globally already, setup it.
    const initialized = window.Waves !== undefined;
    if (initialized) {
      Api.Waves.Setup();
    }
    commit(Mutations.SET_WAVES_KEEPER_INIT, initialized);
  },
  async setPopupVisibility({ commit }, { popup, visible }) {
    commit(Mutations.SET_POPUP_VISIBILITY, {
      popup,
      visible,
    });
  },
  async getPrize({ commit, getters, dispatch }) {
    // Sign payout transaction
    try {
      commit(Mutations.SET_PROCESSING, true);

      const isValid = await dispatch(Actions.KEEPER_VALIDATION);

      if (isValid) {
        const { playerAddress } = getters;
        if (playerAddress !== getters.lastPaymentAddress) {
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups.NOT_A_WINNER,
            visible: true,
          });
          // Second - you're not a winner ;(
          return;
        }

        const payout = createTransfer({
          senderPublicKey: config.publicKey,
          amount: getters.balance - config.txPayout.fee,
          recipient: playerAddress,
          fee: config.txPayout.fee,
        });
        await Api.Node.broadcast(payout);
      }
    } catch (e) {
      console.error(e);
    } finally {
      commit(Mutations.SET_PROCESSING, false);
    }
  },
  //
  // Actions for internal usage only:
  //
  async [Actions.KEEPER_VALIDATION]({ commit, getters, dispatch }) {
    try {
      // If User is not authenticated:
      if (!getters.isAuthenticated) {
        await dispatch(Actions.AUTHENTICATE);
      }

      const state = await Api.Waves.publicState();
      const { network: { code } } = state;
      if (code !== config.api.network) {
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.SWITCH_NETWORK,
          visible: true,
        });

        return false;
      }

      return true;
    } catch (e) {
      if (e.code && e.code === 14) {
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.ADD_ACCOUNT_FIRST,
          visible: true,
        });
      }

      return false;
    }
  },
  async [Actions.LIFECYCLE]({ commit, getters, dispatch }) {
    const start = new Date();
    try {
      const balance = await Api.getAvailableBalance(config.address);
      commit(Mutations.SET_BALANCE, balance);

      // Get last payment from the store:
      const lastPaymentTx = await Api.getLastPaymentTx();
      if (getters.lastPaymentTx !== lastPaymentTx) {
        commit(Mutations.SET_LAST_PAYMENT_TX, lastPaymentTx);

        const heightToGetMoney = await Api.getHeightToGetMoney();
        commit(Mutations.SET_HEIGHT_TO_GET_MONEY, heightToGetMoney);

        const lastPaymentAddress = await Api.getAddressByTxId(lastPaymentTx);
        commit(Mutations.SET_LAST_PAYMENT_ADDRESS, lastPaymentAddress);

        const lastPaymentsAddresses = await Api.getLastPaymentsAddresses();
        commit(Mutations.SET_LAST_PAYMENTS_ADDRESSES, lastPaymentsAddresses);
      }

      //
      // R = 1440
      // [-------------h---------]
      // C = 1240
      // [---------------]
      //                 W = 200
      //                 (-------)
      //
      // H - Height on start
      // R - Round [0 - 1440]
      // C - Competition [0 - 1240]
      // W - Withdraw (1240 - 1440)
      // h - current height of BC
      //
      // Is C? W?
      // isC: 0 <= ((h - H) % R) <= (C / R)
      // isW: (C / R) < ((h - H) % R) <= 1
      //

      const heightCurrent = await Api.getCurrentHeight();
      if (getters.heightCurrent !== heightCurrent) {
        commit(Mutations.SET_HEIGHT_CURRENT, heightCurrent);

        // Update game round:
        const round = roundFromHeight(heightCurrent);
        commit(Mutations.SET_GAME_ROUND, round);

        // Update game stage:
        await dispatch(Actions.UPDATE_GAME_STAGE);
        if (getters.stage === GameStage.WITHDRAW) {
          await dispatch(Actions.UPDATE_COUNTDOWN);
        }

        // Update USD equivalent:
        const lastPriceUSD = await Api.getLastPriceUSD();
        commit(Mutations.SET_LAST_PRICE_USD, lastPriceUSD);
      }

      // If withdraw stage:
      if (getters.stage === GameStage.WITHDRAW) {
        const { data } = await Api.getAvailableBalanceOnWithdraw(
          getters.heightCurrent - getters.heightOnStageSwitch,
        );
        if (data.length > 0) {
          const { amount, height } = data[0].data;
          if (roundFromHeight(height) === getters.round) {
            commit(Mutations.SET_BALANCE_ON_WITHDRAW, amount * config.wave);
          } else {
            commit(Mutations.SET_BALANCE_ON_WITHDRAW, getters.balance);
          }
        } else {
          commit(Mutations.SET_BALANCE_ON_WITHDRAW, getters.balance);
        }
      }
    } catch (e) {
      console.error(e);
    } finally {
      const time = new Date().getMilliseconds() - start.getMilliseconds();
      if (time < config.updateFrequency) {
        await wait(config.updateFrequency - time);
      }
      dispatch(Actions.LIFECYCLE);
    }
  },
  async [Actions.AUTHENTICATE]({ commit }) {
    await Api.Waves.auth({ data: config.app.name });

    // After auth get the address:
    const { account: { address, publicKey } } = await Api.Waves.publicState();

    // Set player address for future games:
    commit(Mutations.SET_PLAYER_ADDRESS, address);
    commit(Mutations.SET_PLAYER_PUBLICKEY, publicKey);
  },
  async [Actions.PLAY_PROCESSING]({ commit, getters, dispatch }) {
    try {
      const txTWithId = createTransfer({
        senderPublicKey: getters.playerPublicKey,
        recipient: config.address,
        fee: config.txPayment.fee * config.wave,
        amount: (config.txPayment.amount) * config.wave + config.txPayment.default * config.wave,
      });

      const txT = {
        type: 4,
        data: {
          senderPublicKey: getters.playerPublicKey,
          amount: {
            assetId: 'WAVES',
            coins: (config.txPayment.amount) * config.wave + config.txPayment.default * config.wave,
          },
          fee: {
            assetId: 'WAVES',
            coins: config.txPayment.fee * config.wave,
          },
          recipient: config.address,
          timestamp: txTWithId.timestamp,
        },
      };

      const txDWithId = createData({
        senderPublicKey: config.publicKey,
        fee: config.txData.fee * config.wave,
        data: [{
          key: txTWithId.id,
          value: 'used',
          type: 'string',
        }, {
          key: config.fields.lastPayment,
          value: txTWithId.id,
          type: 'string',
        }, {
          key: config.fields.heightToGetMoney,
          value: getters.heightCurrent + config.blocksToIncreaseHeight,
          type: 'integer',
        }],
      });

      const txDAdditionalWithId = createData({
        senderPublicKey: config.publicKey,
        fee: config.txData.fee * config.wave,
        data: [{
          key: txTWithId.id,
          value: 'used',
          type: 'string',
        }, {
          key: config.fields.lastPayment,
          value: txTWithId.id,
          type: 'string',
        }, {
          key: config.fields.heightToGetMoney,
          value: getters.heightCurrent + config.blocksToIncreaseHeight,
          type: 'integer',
        }],
      });

      const txD1 = {
        type: 12,
        data: {
          senderPublicKey: config.publicKey,
          fee: {
            assetId: 'WAVES',
            coins: config.txData.fee * config.wave,
          },
          data: [{
            key: txTWithId.id,
            value: 'used',
            type: 'string',
          }, {
            key: config.fields.lastPayment,
            value: txTWithId.id,
            type: 'string',
          }, {
            key: config.fields.heightToGetMoney,
            value: getters.heightCurrent + config.blocksToIncreaseHeight,
            type: 'integer',
          }],
          timestamp: txDWithId.timestamp,
        },
      };

      const txD2 = {
        ...txD1,
        data: {
          ...txD1.data,
          timestamp: txDAdditionalWithId.timestamp,
        },
      };

      const [
        txPaymentSigned,
        txDataSigned1,
        txDataSigned2,
      ] = await Api.Waves.signTransactionPackage([txT, txD1, txD2]);
      const txPayment = JSON.parse(txPaymentSigned);
      txPayment.id = txTWithId.id;

      const [txData, txDataAdditional] = [JSON.parse(txDataSigned1), JSON.parse(txDataSigned2)];
      txData.id = txDWithId.id;
      txDataAdditional.id = txDAdditionalWithId.id;

      const txTransferBroadcast = await Api.Node.broadcast(txPayment);

      // Add data to continuing pool:
      commit(Mutations.DATATX_POOL_ADD_DATA, txData);
      commit(Mutations.DATATX_POOL_ADD_DATA, txDataAdditional);

      // while tx is not assigned to the BC:
      let txPaymentInBlockchain = null;
      do {
        await wait(1000);
        try {
          // case #1: exception here - ERR_CONNECTION, retry
          // case #2: null object - 404, tx not in BC, retry
          // eslint-disable-next-line
          txPaymentInBlockchain = await Api.Node.getTransactionById(txTransferBroadcast.id);
        } catch (e) {
          // ignore error
        }
      } while (!txPaymentInBlockchain);

      // Wait 10 seconds before first dataTx:
      await wait(10000);
      await Api.Node.broadcast(txData);
      // Start data tx continuing push:
      dispatch(Actions.CONTINUE_PUSH_DATA, txData);

      // Wait 10 seconds before second dataTx:
      await wait(10000);
      await Api.Node.broadcast(txDataAdditional);
      // Start data tx additional continuing push:
      dispatch(Actions.CONTINUE_PUSH_DATA, txDataAdditional);
    } catch (e) {
      throw e;
    }
  },
  async [Actions.UPDATE_COUNTDOWN]({ commit, getters }) {
    const hours = Math.floor((getters.heightOnRoundFinish - getters.heightCurrent) / 60);
    const minutes = (getters.heightOnRoundFinish - getters.heightCurrent) - hours * 60;

    commit(Mutations.SET_COUNTDOWN, {
      hours,
      minutes,
    });
  },
  async [Actions.UPDATE_GAME_STAGE]({ commit, getters }) {
    const {
      heightCurrent,
      heightToGetMoney,
      heightOnRoundStart,
      heightOnRoundFinish,
      round, isGameOver,
    } = getters;

    const {
      blocksPerCompetition,
      blocksPerWithdraw,
      blocksToIncreaseHeight,
    } = config;

    const offset = (heightCurrent - config.blocksOnGameStart) % config.blocksPerRound;
    const isPaymentInPrevRound = roundFromHeight(heightToGetMoney) < round;

    // Conditions on the start of the round, in first blocks:
    if (offset <= blocksToIncreaseHeight) {
      if (isPaymentInPrevRound) {
        commit(Mutations.SET_GAME_STAGE, GameStage.COMPETITION);
        commit(
          Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
          1 + heightOnRoundFinish - blocksPerWithdraw,
        );
        commit(
          Mutations.SET_HEIGHT_TO_GET_MONEY,
          heightOnRoundStart + blocksToIncreaseHeight,
        );
      } else if (isGameOver) {
        commit(Mutations.SET_GAME_STAGE, GameStage.WITHDRAW);
        commit(
          Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
          1 + heightToGetMoney,
        );
      } else {
        commit(Mutations.SET_GAME_STAGE, GameStage.COMPETITION);
        commit(
          Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
          1 + heightOnRoundFinish - blocksPerWithdraw,
        );
      }
    } else if (offset <= blocksPerCompetition) {
      if (isGameOver) {
        commit(Mutations.SET_GAME_STAGE, GameStage.WITHDRAW);

        if (isPaymentInPrevRound) {
          commit(
            Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
            1 + heightOnRoundStart + blocksToIncreaseHeight,
          );
        } else {
          if (heightToGetMoney < heightOnRoundFinish - blocksPerWithdraw) {
            commit(
              Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
              1 + heightToGetMoney,
            );
          } else {
            commit(
              Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
              1 + heightOnRoundFinish - blocksPerWithdraw,
            );
          }
        }
      } else {
        commit(Mutations.SET_GAME_STAGE, GameStage.COMPETITION);
        commit(
          Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
          1 + heightOnRoundFinish - blocksPerWithdraw,
        );
      }
    } else {
      commit(Mutations.SET_GAME_STAGE, GameStage.WITHDRAW);
      if (isPaymentInPrevRound) {
        commit(
          Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
          1 + heightOnRoundStart + blocksToIncreaseHeight,
        );
      } else {
        if (heightToGetMoney < heightOnRoundFinish - blocksPerWithdraw) {
          commit(
            Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
            1 + heightToGetMoney,
          );
        } else {
          commit(
            Mutations.SET_HEIGHT_ON_STAGE_SWITCH,
            1 + heightOnRoundFinish - blocksPerWithdraw,
          );
        }
      }
    }

    const { stage } = getters;
    commit(Mutations.SET_POPUP_VISIBILITY, {
      popup: Popups.WITHDRAW,
      visible: stage === GameStage.WITHDRAW,
    });
  },
  async [Actions.CONTINUE_PUSH_DATA]({ commit }, data) {
    const thresholdTime = 1000 * 60 * 3;
    const entryTime = new Date().getTime();

    if (entryTime - data.timestamp <= thresholdTime) {
      await new Promise((async (resolve) => {
        let repeats = 30;
        do {
          await wait(5000);
          try {
            // case #1: exception here - ERR_CONNECTION, retry
            // case #2: null object - 404, tx is not allowed by account-script, retry
            // eslint-disable-next-line
            await Api.Node.broadcast(data);
          } catch (e) {
            // ignore error:
          } finally {
            repeats -= 1;
          }
        } while (repeats >= 0 || new Date().getTime() - data.timestamp <= thresholdTime);

        commit(Mutations.DATATX_POOL_REMOVE_DATA, data.id);
        resolve();
      }));
    } else {
      commit(Mutations.DATATX_POOL_REMOVE_DATA, data.id);
    }
  },
};
