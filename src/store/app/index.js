/* eslint-disable */
import * as Mutations from './mutation-types';
import actions from './actions';
import config from '@/config';
import { Popups, GameStage } from '@/utils';

const state = {
  round: 0,
  stage: null,
  heightOnStageSwitch: 0,
  heightToGetMoney: 0,
  heightCurrent: 0,
  wavesKeeperInitialized: false,
  lastPaymentsAddresses: [],
  lastPaymentAddress: null,
  lastPaymentTx: null,
  lastPriceUSD: 0,
  loadingProgress: 0,
  playerAddress: null,
  playerPublicKey: null,
  processing: false,
  popupsVisibility: {
    [Popups.INFORMATION]: false,
    [Popups.ADD_ACCOUNT_FIRST]: false,
    [Popups.WITHDRAW]: false,
    [Popups.SWITCH_NETWORK]: false,
    [Popups.NOT_A_WINNER]: false,
    [Popups.INSUFFICIENT_FUNDS]: false,
    [Popups.ALREADY_GOT_PRIZE]: false,
  },
  balance: 0,
  balanceOnWithdraw: 0,
  balanceInUSD: 0,
  countdown: {
    hours: 0,
    minutes: 0,
  },
  dataTxPool: JSON.parse(localStorage.getItem('dataTxPool')) || [],
};

const mutations = {
  [Mutations.DATATX_POOL_ADD_DATA](state, payload) {
    state.dataTxPool.push(payload);
    localStorage.setItem('dataTxPool', JSON.stringify(state.dataTxPool));
  },
  [Mutations.DATATX_POOL_REMOVE_DATA](state, payload) {
    const id = state.dataTxPool.findIndex(d => d.id === payload);
    if (id > -1) {
      state.dataTxPool.splice(id, 1);
    }
    localStorage.setItem('dataTxPool', JSON.stringify(state.dataTxPool));
  },
  [Mutations.SET_PLAYER_ADDRESS](state, payload) {
    state.playerAddress = payload;
  },
  [Mutations.SET_PLAYER_PUBLICKEY](state, payload) {
    state.playerPublicKey = payload;
  },
  [Mutations.SET_HEIGHT_ON_STAGE_SWITCH](state, payload) {
    state.heightOnStageSwitch = payload;
  },
  [Mutations.SET_GAME_STAGE](state, payload) {
    state.stage = payload;
  },
  [Mutations.SET_GAME_ROUND](state, payload) {
    state.round = payload;
  },
  [Mutations.SET_COUNTDOWN](state, payload) {
    state.countdown = payload;
  },
  [Mutations.SET_PERMISSION_TO_BET](state, payload) {
    state.permissionToBet = payload;
  },
  [Mutations.SET_WAVES_KEEPER_INIT](state, payload) {
    state.wavesKeeperInitialized = payload;
  },
  [Mutations.SET_LAST_PAYMENTS_ADDRESSES](state, payload) {
    state.lastPaymentsAddresses = payload;
  },
  [Mutations.SET_LAST_PAYMENT_ADDRESS](state, payload) {
    state.lastPaymentAddress = payload;
  },
  [Mutations.SET_LAST_PAYMENT_TX](state, payload) {
    state.lastPaymentTx = payload;
  },
  [Mutations.SET_HEIGHT_TO_GET_MONEY](state, payload) {
    state.heightToGetMoney = payload;
  },
  [Mutations.SET_HEIGHT_CURRENT](state, payload) {
    if (state.heightCurrent <= payload) {
      state.heightCurrent = payload;
    }
  },
  [Mutations.SET_PROCESSING](state, payload) {
    state.processing = payload;
  },
  [Mutations.SET_BALANCE](state, payload) {
    state.balance = payload;
  },
  [Mutations.SET_BALANCE_ON_WITHDRAW](state, payload) {
    state.balanceOnWithdraw = payload;
  },
  [Mutations.SET_LAST_PRICE_USD](state, payload) {
    state.lastPriceUSD = payload;
  },
  [Mutations.SET_LOADING_PROGRESS](state, payload) {
    state.loadingProgress = payload;
  },
  [Mutations.SET_POPUP_VISIBILITY](state, { popup, visible }) {
    state.popupsVisibility[popup] = visible;
  }
};

const getters = {
  round: state => state.round,
  stage: state => state.stage,
  balance: state => state.balance,
  balanceOnWithdraw: state => state.balanceOnWithdraw,
  lastPriceUSD: state => state.lastPriceUSD,
  lastPaymentTx: state => state.lastPaymentTx,
  lastPaymentAddress: state => state.lastPaymentAddress,
  playerAddress: state => state.playerAddress,
  playerPublicKey: state => state.playerPublicKey,
  lastPaymentsAddresses: state => state.lastPaymentsAddresses,
  heightOnRoundStart: state => config.blocksOnGameStart + (state.round - 1) * config.blocksPerRound,
  heightOnRoundFinish: state => config.blocksOnGameStart + (state.round) * config.blocksPerRound,
  heightOnStageSwitch: state => state.heightOnStageSwitch,
  heightToGetMoney: state => state.heightToGetMoney,
  heightCurrent: state => state.heightCurrent,
  heightLeft: state => state.heightToGetMoney - state.heightCurrent,
  dataTxPool: state => state.dataTxPool,
  wavesKeeperInitialized: state => state.wavesKeeperInitialized,
  loadingProgress: state => state.loadingProgress,
  countdown: state => state.countdown,
  popupsVisibility: state => state.popupsVisibility,
  isStageWithdraw: state => state.stage === GameStage.WITHDRAW,
  isLoaded: state => state.loadingProgress === 100,
  isGameOver: state => state.heightToGetMoney - state.heightCurrent <= -1,
  isProcessing: state => state.processing,
  isAuthenticated: state => state.playerAddress !== null,
  isAnyPopupExceptWithdrawOpened: state => Object.entries(state.popupsVisibility)
    .filter(k => k[0] !== Popups.WITHDRAW)
    .some(k => k[1] === true)
};

export default {
  mutations,
  getters,
  actions,
  state,
};
