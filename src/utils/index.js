import * as GameStage from './stage';
import * as Popups from './popups';
import config from '@/config';

const invertPromise = promise => new Promise(
  (resolve, reject) => promise.then(reject, resolve),
);

export const firstSuccessful = promises => invertPromise(
  Promise.all(
    promises.map(invertPromise),
  ),
);

export const roundFromHeight = heightCurrent => 1 + Math.trunc(
  (heightCurrent - config.blocksOnGameStart) / config.blocksPerRound,
);

export const progressFromHeight = heightCurrent => (
  ((heightCurrent - config.blocksOnGameStart) % config.blocksPerRound) / config.blocksPerRound
);

export const stageFromHeight = (heightCurrent) => {
  if (progressFromHeight(heightCurrent) <= (config.blocksPerCompetition / config.blocksPerRound)) {
    return GameStage.COMPETITION;
  }

  return GameStage.WITHDRAW;
};

export const wait = ms => new Promise(r => setTimeout(r, ms));

export { GameStage };
export { Popups };
