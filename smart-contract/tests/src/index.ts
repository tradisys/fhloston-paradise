import { api } from './api'
import { TESTNET } from './config'
import { axiosHttp } from './http'

export { TestFlow } from './flow'
export const TestNetApi = api(TESTNET, axiosHttp)

export enum Scenario {
    GOOD,
    BAD,
}

export const Stage = (scenario: Scenario, description: string, fn: () => void): void => {
    it(`[${scenario}]: ${description}`, fn, 1000 * 60 * 10)
}
