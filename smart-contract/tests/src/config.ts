export interface IApiConfig {
    base: string
    tx: string
    chainId: string
}

export const MAINNET: IApiConfig = {
    base: 'https://testnodes.wavesnodes.com/',
    tx: 'https://api.testnet.wavesplatform.com/v0/',
    chainId: 'W',
}

export const TESTNET: IApiConfig = {
    base: 'https://testnodes.wavesnodes.com/',
    tx: 'https://api.testnet.wavesplatform.com/v0/',
    chainId: 'T',
}
