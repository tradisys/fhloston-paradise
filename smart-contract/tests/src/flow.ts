import { IApiConfig } from './config'
import { IWavesApi } from './api'
import { address, publicKey } from '@waves/waves-crypto'
import { Account, Address, PublicKey, Seed } from './basics'
import { data, setScript as setScriptTx, transfer } from '@waves/waves-transactions'
import fees from './fees'
import { DataTransaction, SetScriptTransaction, TransferTransaction } from './transactions'
import { isSeed, randomSeed } from './utils'

export class TestFlow {
    private readonly hostAccount: Account
    private readonly hostSeed: string
    private readonly hostAddress: string
    private readonly hostPublicKey: string
    private readonly config: IApiConfig
    private readonly api: IWavesApi
    private readonly accounts: Account[]

    constructor(seed: string, api: IWavesApi) {
        this.api = api
        this.config = api.config()
        this.hostSeed = seed
        this.hostAddress = address(seed, this.config.chainId)
        this.hostPublicKey = publicKey(seed)
        this.hostAccount = {
            address: this.hostAddress,
            publicKey: this.hostPublicKey,
            seed: this.hostSeed,
        }
        this.accounts = []
    }

    private async hasEnoughBalance(balance: number) {
        const hostBalance = await this.api.getBalance(this.hostAddress)
        if (hostBalance < balance) {
            throw new Error(`Not enough balance on host ${this.hostAddress} to run test`)
        }
    }

    private async sendFundsToAddressAndWait(address: string, amount: number) {
        const alreadyOnBalance = await this.api.getBalance(address)
        const missing = amount - alreadyOnBalance
        await this.hasEnoughBalance(missing + fees.REGULAR)
        const t = transfer({
            senderPublicKey: this.hostPublicKey,
            amount: missing,
            recipient: address,
        }, this.hostSeed)
        await this.api.broadcast(t)
        await this.api.waitForBalance(address, alreadyOnBalance, amount)
    }

    private async randomSeedWithBalance(balance: number): Promise<string> {
        const seed = randomSeed()
        const a = address(seed, this.config.chainId)
        await this.sendFundsToAddressAndWait(a, balance)

        return seed
    }

    public getHostAccount(): Account {
        return this.hostAccount
    }

    public getApi(): IWavesApi {
        return this.api
    }

    public async returnFundsToHost() {
        const asyncForEach = async <T>(array: T[], callback: (value: T, index: number, array: T[]) => void) => {
            for (let index = 0; index < array.length; index++) {
                await callback(array[index], index, array)
            }
        }
        await asyncForEach(this.accounts, async (acc) => {
            const account = <Address & PublicKey & Seed>(acc)
            const amount = await this.api.getBalance(account.address)
            if (amount > 0) {
                const t = transfer({
                    senderPublicKey: account.publicKey,
                    amount: amount - fees.REGULAR,
                    recipient: this.hostAddress,
                }, account.seed)
                await this.api.broadcast(t)
            } else {
                this.accounts.splice(this.accounts.indexOf(acc), 1)
            }
        })
    }

    public async getRandomAccountWithBalance(balance: number = 0): Promise<Account> {
        const seed = balance > 0 ? await this.randomSeedWithBalance(balance) : randomSeed()

        return {
            seed,
            address: address(seed, this.config.chainId),
            publicKey: publicKey(seed),
        }
    }

    public addAccountRefund(...accounts: Account[]) {
        this.accounts.push(...accounts)
    }

    public getRandomAccount(): Account {
        const seed = randomSeed()

        return {
            seed,
            address: address(seed, this.config.chainId),
            publicKey: publicKey(seed),
        }
    }

    public async setScript(seed: Seed, script: string): Promise<SetScriptTransaction> {
        const tx = setScriptTx({script, chainId: this.config.chainId}, seed.seed)

        return await this.api.broadcastAndWait(tx) as SetScriptTransaction
    }

    public async transferWaves(amount: number, from: Account, to: Account): Promise<TransferTransaction> {
        const initial = await this.api.getBalance((<Address>to).address)
        const tx = transfer({amount, recipient: (<Address>to).address}, (<Seed>from).seed)
        const btx = await this.api.broadcastAndWait(tx) as TransferTransaction

        await this.api.waitForBalance((<Address>to).address, initial, amount)

        return btx
    }

    public async waitHeight(height: number) {
        let h = 0
        while (h < height) {
            await this.wait(1000 * 5)
            h = await this.api.getHeight()
        }
    }

    public async setData(account: PublicKey | Seed, map: Record<string, string | number | boolean | Buffer | Uint8Array | number[]>): Promise<DataTransaction> {
        if (isSeed(account)) {
            const tx = data({data: Object.keys(map).map(key => ({key, value: map[key]}))}, account.seed)
            return await this.api.broadcastAndWait(tx) as DataTransaction
        } else {
            const tx = data({
                additionalFee: 400000,
                senderPublicKey: account.publicKey,
                data: Object.keys(map).map(key => ({key, value: map[key]})),
            })
            return await this.api.broadcastAndWait(tx) as DataTransaction
        }
    }

    public async wait(milliseconds: number): Promise<void> {
        return new Promise(resolve => setTimeout(resolve, milliseconds))
    }
}
