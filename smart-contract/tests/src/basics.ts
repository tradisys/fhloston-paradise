export interface Seed {
    seed: string
}

export interface PublicKey {
    publicKey: string
}

export interface Address {
    address: string
}

export type Account = Seed | PublicKey | Address
