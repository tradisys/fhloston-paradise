import axios from 'axios'
import { IHttpClient } from './api'

export const axiosHttp: IHttpClient = {
    get: <T>(url: string) => axios.get<T>(url).then(x => x.data as T),
    post: <T>(url: string, data: any) => axios.post<T>(url, data).then(x => x.data as T),
}
