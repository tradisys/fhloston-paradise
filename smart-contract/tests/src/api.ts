import { TTx as Tx, WithId } from '@waves/waves-transactions'
import { IApiConfig } from './config'
import { retry } from './utils'

type TTx = Tx & WithId

export interface IHttpClient {
    get: <T>(url: string) => Promise<T>
    post: <T>(url: string, data: any) => Promise<T>
}

export interface IWavesApi {
    getHeight(): Promise<number>

    getTxById(txId: string): Promise<TTx>

    waitForTx(txId: string): Promise<TTx>

    waitForBalance(address: string, initial: number, increased: number): void

    broadcast(tx: TTx): Promise<TTx>

    broadcastAndWait(tx: Tx): Promise<TTx>

    compile(script: string): Promise<string>

    getBalance(address: string): Promise<number>

    getStateAddress<T>(address: string, key: string): Promise<T>

    config(): IApiConfig
}

export const api = (config: IApiConfig, http: IHttpClient): IWavesApi => {
    const get = <T>(endpoint: string): Promise<T> => retry(() => http.get<T>(config.base + endpoint), 5, 1000)

    const post = <T>(endpoint: string, data: any): Promise<T> => retry(() => http.post<T>(config.base + endpoint, data), 5, 1000)

    const waitForTx = async (txId: string): Promise<TTx> => retry(() => {
        return getTxById(txId)
    }, 500, 1000)

    const waitForBalance = async (address: string, initial: number, increased: number) => retry(() => {
        return new Promise((resolve, reject) => {
            getBalance(address)
                .then(b => {
                    if (b == initial + increased) {
                        resolve()
                    } else {
                        reject()
                    }
                })
                .catch(() => reject())
        })
    }, 500, 1000, true)

    const getHeight = async () => get<{ height: number }>('blocks/last').then(x => x.height)

    const getTxById = async (txId: string): Promise<TTx> => get<TTx>(`transactions/info/${txId}`)

    const broadcast = async (tx: Tx): Promise<TTx & WithId> => post<TTx>('transactions/broadcast', tx)

    const broadcastAndWait = async (tx: Tx): Promise<TTx> => {
        const r = await broadcast(tx)
        await waitForTx(r.id)
        return r
    }

    const compile = async (script: string): Promise<string> => post<{ script: string }>('utils/script/compile', script).then(x => x.script)

    const getStateAddress = <T>(address: string, key: string): Promise<T> =>
        get<T>(`addresses/data/${address}/${key}`).then(x => x as T)

    const getBalance = (address: string): Promise<number> =>
        get<{ available: number }>(`addresses/balance/details/${address}`).then(x => x.available)

    return {
        getHeight,
        getTxById,
        broadcast,
        waitForTx,
        waitForBalance,
        broadcastAndWait,
        compile,
        getStateAddress,
        getBalance,
        config: () => config,
    }
}
