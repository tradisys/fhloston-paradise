import { randomBytes } from 'crypto'
import { Account, PublicKey, Seed } from './basics'

export const delay = (millis: number): Promise<{}> =>
    new Promise((resolve, _) => {
        setTimeout(resolve, millis)
    })

const wrapError = (error: any) => {
    let er
    if (error && error.response && error.response.data) {
        switch (error.response.data.error) {
            case 112:
                er = {
                    code: 112,
                    message: error.response.data.message,
                    tx: error.response.data.tx,
                }
                break
            case 199: //script too lagre
                er = {
                    code: 199,
                    message: error.response.data.message,
                }
                break
            case 306: //error while executing
                er = {
                    code: 306,
                    message: error.response.data.message,
                    tx: error.response.data.transaction,
                    vars: error.response.data.vars.reduce((a: [], b: []) => [...a, ...b], []),
                }
                break
            case 307:
                er = {
                    code: 307,
                    message: error.response.data.message,
                    tx: error.response.data.transaction,
                    vars: error.response.data.vars.reduce((a: [], b: []) => [...a, ...b], []),
                }
                break
            default:
                er = error
        }

        return er
    }
}

export const retry = async <T>(action: () => Promise<T>, limit: number, delayAfterFail: number, ignoreErrors: boolean = false): Promise<T> => {
    try {
        return await action()
    } catch (error) {
        if (!ignoreErrors) {
            const er = wrapError(error)
            if (limit < 1 || er.code) {
                throw er
            }
        }
    }

    await delay(delayAfterFail)
    return await retry(action, limit - 1, delayAfterFail, ignoreErrors)
}


export const randomSeed = (): string => randomBytes(32).toString('hex')

export const isSeed = (account: Account): account is Seed => (<any>account).seed !== undefined

export const isPublicKey = (account: Account): account is PublicKey => (<any>account).publicKey !== undefined


