import { TestNetApi, TestFlow } from '../src'
import { Address, PublicKey, Seed } from '../src/basics'
import { fomoSmartContract } from './fomo'
import fees from '../src/fees'
import { data, transfer } from '@waves/waves-transactions'
import { TransferTransaction } from '../src/transactions'

describe('FOMO flow', () => {
    // Predefined constants and flow:
    const wave = 100000000
    const minFee = 500000
    const minHeight = 13
    const maxHeight = 17
    const heightStep = 15
    const flow = new TestFlow('YOUR_SEED', TestNetApi)

    // Var initialized after smart contract deployment
    let topHeight = 0
    let txPaymentAlreadySent: TransferTransaction

    // Accounts:
    const fomoAccount = flow.getRandomAccount()
    const aliceAccount = flow.getRandomAccount()
    const bobAccount = flow.getRandomAccount()

    // Add accounts to auto withdraw after tests:
    flow.addAccountRefund(aliceAccount, bobAccount, fomoAccount)

    // Prints accounts to stdout:
    console.log('FOMO account', fomoAccount)
    console.log('Alice account', aliceAccount)
    console.log('Bob account', bobAccount)

    console.log('deploy FOMO contract ...')
    it('deploy FOMO contract', async () => {
        // Fomo deployment:
        const ttx = await flow.transferWaves(fees.SET_SCRIPT + fees.REGULAR, flow.getHostAccount(), fomoAccount)
        txPaymentAlreadySent = ttx

        const height = await flow.getApi().getHeight()

        topHeight = height

        // Send data as initial state:
        await expect(
            flow.getApi().broadcastAndWait(data({
                senderPublicKey: '',
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>fomoAccount).seed))
        ).resolves.toBeDefined()

        const compiledScript = await flow.getApi().compile(fomoSmartContract(topHeight, minHeight, maxHeight))
        await expect(
            flow.setScript(<Seed>fomoAccount, compiledScript)
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making default bet (1.005 waves) ...')
    it('Alice making default bet (1.005 waves)', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making default bet (1.005 waves) ...')
    it('Bob making default bet (1.005 waves)', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making special bet 5.1234298 waves again ...')
    it('Alice making special bet 5.1234298 waves again', async () => {
        const bet = 5.1234298 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making special bet (3.13234 waves with minFee - 1 wavelet) but will be failed ...')
    it('Bob making special bet (3.13234 waves with minFee - 1 wavelet) but will be failed', async () => {
        const bet = 3.13234 * wave
        await expect(
            flow.transferWaves(bet + minFee - 1 + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee - 1, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee - 1,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob trying to double-spend data (using tx which has been sent already), will be failed ...')
    it('Bob trying to double-spend data (using tx which has been sent already), will be failed', async () => {
        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: txPaymentAlreadySent.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: txPaymentAlreadySent.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making special bet (7.15 waves with fee = 7.15 (all mount)) but will be failed ...')
    it('Bob making special bet (7.15 waves with fee = 7.15 (all mount)) but will be failed', async () => {
        const bet = 7.15 * wave
        await expect(
            flow.transferWaves(bet + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making default bet with wrong lastPayment, but will be failed')
    it('Bob making default bet with wrong lastPayment, but will be failed', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet * minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: 'GD4vTVcUK7KDbNJQhtvf5zor6AxYKPY9xzMDD4dJzehr'},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making bet, but with wrong height (minHeight - 1), will be failed ...')
    it('Bob making bet, but with wrong height (minHeight - 1), will be failed', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + minHeight - 1},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making bet, but with wrong height (maxHeight + 1), will be failed ...')
    it('Bob making bet, but with wrong height (maxHeight + 1), will be failed', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + maxHeight + 1},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making default bet, but data signed not by Bob, will be failed ...')
    it('Bob making default bet, but data signed not by Bob, will be failed', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + maxHeight + 1},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making bet with maxHeight prolongation ...')
    it('Bob making bet with maxHeight prolongation', async () => {
        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + maxHeight},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making wrong bet (0.521 wave, less than 1 wave), data will be failed ...')
    it('Alice making wrong bet (0.521 wave, less than 1 wave), data will be failed', async () => {
        const bet = 0.521 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob does not make transfer, but sends data with another`s tx id ...')
    it('Bob does not make transfer, but sends data with another`s tx id', async () => {
        const bet = wave
        const height = await flow.getApi().getHeight()
        const wrongTxId = 'GTtcjsy9TXEP1AK4HtMZhtBJpXjHbEq1ZiciYZYScYrZ'

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet * 0.09,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: wrongTxId},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: wrongTxId, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('withdraw money before game finish, will be failed ...')
    it('withdraw money before game finish, will be failed', async () => {
        const fomoBalance = await flow.getApi().getBalance((<Address>fomoAccount).address)

        await expect(
            flow.getApi().broadcastAndWait(transfer({
                amount: fomoBalance - 500000,
                fee: 500000,
                recipient: (<Address>aliceAccount).address,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
            }, (<Seed>aliceAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Bob making bet (10.005 waves, and 9 waves goes to the data fee) ...')
    it('Bob making bet (10.005 waves, and 9 waves goes to the data fee)', async () => {
        const bet = 10 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet - wave,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>bobAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making bet (3 waves, and 3 waves goes to the data fee), will be failed ...')
    it('Alice making bet (3 waves, and 3 waves goes to the data fee), will be failed', async () => {
        const bet = 3 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet - 1,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making bet (3 waves, and 3 - 0.005 waves goes to the data fee), will be failed ...')
    it('Alice making bet (3 waves, and 3 - 0.005 waves goes to the data fee), will be failed', async () => {
        const bet = 3 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: bet - minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('Alice making bet (100 waves + minFee) ...')
    it('Alice making bet (100 waves + minFee)', async () => {
        const bet = 100 * wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), aliceAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, aliceAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
                fee: minFee,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                data: [
                    {key: 'lastPayment', value: ttx.id},
                    {key: 'heightToGetMoney', value: height + heightStep},
                    {key: ttx.id, value: 'used'},
                ],
            }, (<Seed>aliceAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)

    /*it('Bob making default bet (1.005 waves) on topHeight, will be failed', async () => {
        await flow.waitHeight(topHeight)

        const bet = wave
        await expect(
            flow.transferWaves(bet + minFee + fees.REGULAR, flow.getHostAccount(), bobAccount)
        ).resolves.toBeDefined()

        // make transfer to fomoAccount
        const ttx = await flow.transferWaves(bet + minFee, bobAccount, fomoAccount)

        const height = await flow.getApi().getHeight()

        await expect(
            flow.getApi().broadcastAndWait(data({
            fee: minFee,
            senderPublicKey: (<PublicKey>fomoAccount).publicKey,
            data: [
                {key: 'lastPayment', value: ttx.id},
                {key: 'heightToGetMoney', value: height + heightStep},
                {key: ttx.id, value: 'used'},
            ],
        }, (<Seed>bobAccount).seed))).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('withdraw attack - sends 100 waves to winner, on contract will be less 100 waves ...')
    it('withdraw attack - sends 100 waves to winner, on contract will be less 100 waves', async () => {
        const heightToGetMoney = await flow.getApi().getStateAddress<{ value: number }>((<Address>fomoAccount).address, 'heightToGetMoney').then(v => v.value)
        await flow.waitHeight(heightToGetMoney)

        await expect(
            flow.getApi().broadcastAndWait(transfer({
                amount: 100 * wave,
                fee: 500000,
                recipient: (<Address>aliceAccount).address,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
            }, (<Seed>aliceAccount).seed))
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)


    console.log('Alice adding missing waves up to 100 waves on fomo balance ...')
    it('Alice adding missing waves up to 100 waves on fomo balance', async () => {
        const heightToGetMoney = await flow.getApi().getStateAddress<{ value: number }>((<Address>fomoAccount).address, 'heightToGetMoney').then(v => v.value)
        await flow.waitHeight(heightToGetMoney)

        const alreadyOnBalance = await flow.getApi().getBalance((<Address>fomoAccount).address)
        const missing = 100 * wave + 500000 - alreadyOnBalance

        // make transfer from Alice to Fomo
        await expect(
            flow.transferWaves(missing, aliceAccount, fomoAccount)
        ).resolves.toBeDefined()

    }, 1000 * 60 * 10)*/

    console.log('withdraw attack - sends by 1 wavelet 10 times to winner, each must be failed ...')
    it('withdraw attack - sends by 1 wavelet 10 times to winner, each must be failed', async () => {
        const heightToGetMoney = await flow.getApi().getStateAddress<{ value: number }>((<Address>fomoAccount).address, 'heightToGetMoney').then(v => v.value) + 1
        await flow.waitHeight(heightToGetMoney)

        for (let i = 0; i < 10; i++) {
            await expect(
                flow.getApi().broadcastAndWait(transfer({
                    amount: 1,
                    fee: 500000,
                    recipient: (<Address>aliceAccount).address,
                    senderPublicKey: (<PublicKey>fomoAccount).publicKey,
                }))
            ).rejects.toBeDefined()

        }
    }, 1000 * 60 * 10)

    console.log('withdraw money not to winner, will be failed ...')
    it('withdraw money not to winner, will be failed', async () => {
        const heightToGetMoney = await flow.getApi().getStateAddress<{ value: number }>((<Address>fomoAccount).address, 'heightToGetMoney').then(v => v.value)
        await flow.waitHeight(heightToGetMoney)

        const fomoBalance = await flow.getApi().getBalance((<Address>fomoAccount).address)

        await expect(
            flow.getApi().broadcastAndWait(transfer({
                amount: fomoBalance - 500000,
                fee: 500000,
                recipient: (<Address>bobAccount).address,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
            }, (<Seed>bobAccount).seed))
        ).rejects.toBeDefined()

    }, 1000 * 60 * 10)

    console.log('withdraw money to winner Alice, signed by Bob (could be anybody) ...')
    it('withdraw money to winner Alice, signed by Bob (could be anybody)', async () => {
        const heightToGetMoney = await flow.getApi().getStateAddress<{ value: number }>((<Address>fomoAccount).address, 'heightToGetMoney').then(v => v.value)
        await flow.waitHeight(heightToGetMoney)

        const fomoBalance = await flow.getApi().getBalance((<Address>fomoAccount).address)

        await expect(
            flow.getApi().broadcastAndWait(transfer({
                amount: fomoBalance - 500000,
                fee: 500000,
                recipient: (<Address>aliceAccount).address,
                senderPublicKey: (<PublicKey>fomoAccount).publicKey,
            }, (<Seed>bobAccount).seed))
        ).resolves.toBeDefined()

        await flow.transferWaves(fomoBalance - 500000 - fees.REGULAR, aliceAccount, flow.getHostAccount())
    }, 1000 * 60 * 10)

    afterAll(async (done) => {
        try {
            await flow.returnFundsToHost()
        } catch (e) {
            console.log(e)
        }
        done()
    }, 1000 * 60 * 10)
})
