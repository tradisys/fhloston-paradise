export const fomoSmartContract = (
    blocksOnGameStart: number,
    minHeightDiff: number = 12,
    maxHeightDiff: number = 17
): string => `
# transactions' amounts and fees
let minBetAmount = 100000000
let minFee = 500000

# interval of the allowed height increase
let minHeightDiff = 13 # 13 (exclusive)
let maxHeightDiff = 17 # 17 (exclusive)

let blocksOnGameStart = ${blocksOnGameStart}
let blocksPerRound = 1440
let blocksPerCompetition = 1240

# calculate game's state
let offset = height - blocksOnGameStart
let roundNum = offset / blocksPerRound
let roundHeight = offset % blocksPerRound
let roundHeightStart = roundNum * blocksPerRound + blocksOnGameStart
let roundCompetitionEnd = roundHeightStart + blocksPerCompetition

# this account's address
let SELF = tx.sender

# extract the current height to get money from this account's state
let heightToGetMoney = extract(getInteger(SELF, "heightToGetMoney"))

# Stages
let isWithdrawByWin = height >= heightToGetMoney && heightToGetMoney > roundHeightStart
let isWithdrawByTime = roundHeight > blocksPerCompetition
let isWithdraw = isWithdrawByWin || isWithdrawByTime
let isCompetition = !isWithdraw

match (tx) {
    case dtx: DataTransaction =>
        # extract the provided height to get money from the DataTransaction
        let newHeightToGetMoney = extract(getInteger(dtx.data, "heightToGetMoney"))

        # extract the payment transaction's id from the DataTransaction
        let paymentTxId = extract(getString(dtx.data, "lastPayment"))
        let paymentTxIdVec = fromBase58String(paymentTxId)

        # extract the provided payment transaction from the blockchain
        let paymentTx = transactionById(paymentTxIdVec)

        let paymentTxHeight = extract(transactionHeightById(paymentTxIdVec))

        # check if (heightToGetMoney - height) is in range [minHeightDiff, maxHeightDiff]
        let newHeightToGetMoneyValid =
            if (height + minHeightDiff >= roundCompetitionEnd) then (
                # STRICT LESS
                newHeightToGetMoney <= roundCompetitionEnd
            ) else (
                (newHeightToGetMoney - height) > minHeightDiff && (newHeightToGetMoney - height) < maxHeightDiff
            )

        match (paymentTx)
        {
            case paymentTx: TransferTransaction =>
                # game is ready for playing
                let offsetValid = offset >= 0
                # impossible to use payment from prev. round
                let paymentFromCurrentRound = paymentTxHeight >= roundHeightStart && paymentTxHeight < roundCompetitionEnd
                # check if the payment transaction was not used before (to prevent double-spending)
                let paymentNotUsedBefore = !isDefined(getString(SELF, paymentTxId))
                # check if the payment recipient is this account
                let paymentRecipientValid = paymentTx.recipient == SELF
                # check if the transfer amount exceeds the required minimum
                let paymentAmountValid = paymentTx.amount >= minBetAmount + minFee
                # check if the transferred asset is WAVES
                let paymentAssetValid = !isDefined(paymentTx.assetId)
                # check if the data satisfies the specified format
                let dataFormatValid = size(dtx.data) == 3 && isDefined(getString(dtx.data, paymentTxId))
                # check if the DataTransaction is signed by the payment's sender
                let sigValid = sigVerify(dtx.bodyBytes, dtx.proofs[0], paymentTx.senderPublicKey)
                # fee must be grater than minimal fee and less than payment amount - minimal bet
                let feeValid = dtx.fee >= minFee && dtx.fee <= paymentTx.amount - minBetAmount

                offsetValid
                    && isCompetition # it is impossible to post data during withdraw stage
                    && paymentFromCurrentRound
                    && paymentNotUsedBefore
                    && paymentRecipientValid
                    && paymentAmountValid
                    && paymentAssetValid
                    && newHeightToGetMoneyValid
                    && dataFormatValid
                    && sigValid
                    && feeValid
            case _ => false
        }
    case payout: TransferTransaction =>
        # extract the winner's transaction from the blockchain
        let lastPaymentTx = transactionById(fromBase58String(extract(getString(SELF, "lastPayment"))))
        match (lastPaymentTx)
        {
            case lastPaymentTx: TransferTransaction =>
                # game is ready for playing
                let offsetValid = offset >= 0
                # do not allow payout if new round has been started
                let payoutRoundValid = heightToGetMoney > roundHeightStart
                # check payout balance
                let payoutAmountValid = payout.amount >= wavesBalance(SELF) - minFee
                # check if the receiver is the winner
                let payoutRecipientValid = lastPaymentTx.sender == payout.recipient
                # check if the fee is correct and fee asset is waves
                let payoutFeeValid = payout.fee == minFee && !isDefined(payout.feeAssetId)

                offsetValid
                    && isWithdraw # check if the game has ended
                    && payoutRoundValid
                    && payoutAmountValid
                    && payoutRecipientValid
                    && payoutFeeValid
            case _ => false
        }
    case _ => false
}
`
